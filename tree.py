'''
given a width, print a christmas tree with that width at the base
also, randomly place bulbs on the tree '@'
also place a star 'X' at the top

ie. width = 7

    X
   ###
  #####
 #######
    =
    =
'''
import random
import sys

x = 3
y = 1
width = 25

print " " * (width/2), 
print "X"

while x <= width:
  y = 0 
  print " " * ((width/2) - (x/2)), 
  while y < x:
    r = random.randint(1,10)
    if r != 5:
        print "#", 
        sys.stdout.softspace=0
    else:
        print "@",
        sys.stdout.softspace=0
    y += 1
  print
  x += 2


print " " * ((width / 2) + 1) + "="
print " " * ((width / 2) + 1) + "=" 




	
