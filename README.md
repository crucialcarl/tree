```
#!shell

Live coding exercise I did for a job interview.  Recreated because bored. 

Given a width, print a Christmas tree with that width at the base
Randomly place bulbs on the tree '@'
Place a star 'X' at the top

ie. width = 7

    X
   ###
  #####
 #######
    =
    = 
```